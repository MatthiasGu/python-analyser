import urllib2
import re
from collections import Counter
from bs4 import BeautifulSoup


class Analyser(object):

    def __init__(self):
        self.url = input('Please enter a URL:')
        self.soup = BeautifulSoup(urllib2.urlopen(self.url), "html.parser")

    def visible(self, element):
        if element.parent.name in ['style', 'script', '[document]', 'head', 'title']:
            return False
        elif re.match('<!--.*-->', str(element.encode('utf-8'))):
            return False
        return True

    def print_title(self):
        title = self.soup.title.string
        print "\n -== Page title: " + title

    def print_meta_tags(self):
        tags = self.soup.find_all("meta")
        print "\n -== Page meta tags: "
        for tag in tags:
            print tag

    def print_file_size(self):
        print "\n -== File size:" + str(len(urllib2.urlopen(self.url).read()))

    def get_visible_content(self):
        content = self.soup.findAll(text=True)
        visible_content = filter(self.visible, content)
        return visible_content

    def get_word_counter(self, visible_content):
        words = []
        word_counter = {}
        for line in visible_content:
            for word in line.split():
                if str.isalpha(str(word.encode('utf-8'))):
                    word = str(word.encode('utf-8')).lower()
                    words.append(word)
                    if word not in word_counter:
                        word_counter[word] = 1
                    else:
                        word_counter[word] += 1
        counter = Counter(words)

        return counter, word_counter

    def print_word_count(self, word_counter):
        sum = 0
        unique = 0
        for item in word_counter.itervalues():
            sum += item
            unique += 1
        print "\n -== Word count: " + str(sum)
        print "\n -== Number of unique words: " + str(unique)

    def print_most_common_words(self, counter):
        print "\n -== Most common words: ==-"
        print counter.most_common(5)

    def get_unique_keywords(self):
        unique_keywords = []

        keywords = str(self.soup.find(attrs={"name": "keywords"})['content'])
        for keyword in keywords.split(" "):
            keyword = re.sub('[\W_]+', '', keyword)
            if keyword not in unique_keywords:
                unique_keywords.append(keyword)

        return unique_keywords

    def print_missing_keywords(self, unique_keywords, word_counter):
        print "\n -== Keywords that do not occur in text: ==-"
        for keyword in unique_keywords:
            if keyword not in word_counter.keys():
                print keyword

    def print_links(self):
        print "\n -== Links ==-"
        for link in self.soup.findAll('a'):
            if link.has_attr('href'):
                print str(link.text.encode('utf-8')) + " ->> " +  str(link['href'])

analyser = Analyser()
analyser.print_title()
analyser.print_meta_tags()
analyser.print_file_size()
visible_content = analyser.get_visible_content()
counter, word_counter = analyser.get_word_counter(visible_content)
analyser.print_word_count(word_counter)
analyser.print_most_common_words(counter)
unique_keywords = analyser.get_unique_keywords()
analyser.print_missing_keywords(unique_keywords, word_counter)
analyser.print_links()







